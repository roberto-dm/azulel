<!-- Funcionalidad de suscripción -->
<?php if (isset($_POST['submit'])) require('user.php'); ?>

<footer>
    <div id="FondoAzulPrincipal">
        <div id="backgroundfooter">
            <div id="backgroundblue" class="w3-row">
                <div class="w3-image" id="backgroundclear">
                </div>
                <div class="w3-col l4 k4 w3-center w3-left paddingleft">
                    <div class="w3-row">
                        <h4 class="w3-text-white Courgette">Recibe en tu correo electrónico nuevos consejos, videos y frases</h4>
                        <?php
                            if(isset($mensaje)) { ?>
                                <p class="w3-text-white Courgette <?php echo (!$exitoso) ? "error-footer": "success-footer"; ?>"> <?php echo $mensaje; ?> </p>
                            <?php }
                        ?>
                        <form method="post" action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>">
                            <div class="w3-row-padding w3-margin-bottom">
                                <div class="marginbottom5px">
                                    <input class="w3-round w3-input w3-border Formulario1" style ="color:white;" type="text" name="username" value="<?php echo (isset($_POST['username']) && !$exitoso) ? $_POST['username'] : null;?>" pattern="[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.-]{2,64}" title="No puedes usar caracteres especiales"  placeholder="NOMBRE COMPLETO" autocomplete="off" required>
                                </div>
                                <div>
                                    <input class="w3-round w3-input w3-border Formulario1" style ="color:white;" type="email" name="email" value="<?php echo (isset($_POST['email']) && !$exitoso) ? $_POST['email'] : null;?>" pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$ \''"  placeholder="CORREO ELECTRONICO" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="w3-center" class="marginbottom1">
                                <input type="submit" class="w3-button w3-round-large w3-hover-blue w3-white Copperplate" name="submit" value="SUSCRIBETE">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="w3-col l4 k4 w3-center w3-right">
                    <div class="w3-row" id="ElementoFooter">
                        <h1 class="w3-text-white Raleway"><b>SÍGUENOS EN:</</h1> 
                        <div class="w3-row w3-center" id="iconoss">
                            <div class="w3-col k3 kz3 ky3">
                                <a href="#"><img class="w3-image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/insta.png"></a>
                            </div>
                            <div class="w3-col k3 kz3 ky3">
                                <a href="#"><img class="w3-image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/twitter.png"></a>
                            </div>
                            <div class="w3-col k3 kz3 ky3">
                                <a href="#"><img class="w3-image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/fb.png"></a>
                            </div>
                            <div class="w3-col k3 kz3 ky3">
                                <a href="#"><img class="w3-image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/yt.png"></a>
                            </div>
                        </div>
                        <h2 class="w3-text-white Raleway">@Azulel</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="w3-image" id="BackgroundBlueClear">
        </div>
    </div>
</footer>


        <script> /* SCRIPT PARA EL BOTÓN DEL MENÚ VERSIÓN MÓVIL */
            function botonMenuResponsive() {
                var x = document.getElementById("mResponsive");
                if (x.className.indexOf("w3-show") == -1) {
                    x.className += " w3-show";
                } else { 
                    x.className = x.className.replace(" w3-show", "");
                }
            }
        </script>
        <script>
            var slideIndex = 1;
            showDivs(slideIndex);

            function plusDivs(n) {
                showDivs(slideIndex += n);
            }

            function currentDiv(n) {
                showDivs(slideIndex = n);
            }

            function showDivs(n) {
                var i;
                var x = document.getElementsByClassName("mySlides");
                var dots = document.getElementsByClassName("demo");
                if (n > x.length) { slideIndex = 1 }    
                if (n < 1) { slideIndex = x.length }
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";  
                }
                for (i = 0; i < dots.length; i++) {
                    dots[i].className = dots[i].className.replace(" w3-white", "");
                }
                x[slideIndex-1].style.display = "block";  
                dots[slideIndex-1].className += " w3-white";
            }
        </script>
    </body>
</html>
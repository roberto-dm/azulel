<html style="margin-top: 0px !important;">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title><?php echo bloginfo('name'); ?> - <?php echo get_the_title(); ?></title>
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/stylesheets/w3.css">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/stylesheets/containers.css">
        

		<!-- Fuentes -->
		
		<!-- Conformtaa (Header) -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Comfortaa">

		<!-- Taringe (Footer) --> 
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine">
		<!-- Indie Flower (Footer) -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Indie+Flower">
		<!-- Courgette (Footer) -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Courgette">
		<!-- Raleway (Footer) -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">

		<!-- Dosis (FrontPage) -->
		<link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet"> 

		<!-- Source Serif Pro (FrontPage) -->
		<link href="https://fonts.googleapis.com/css?family=Dosis|Source+Serif+Pro" rel="stylesheet"> 
		
		<!-- Raleway (FrontPage y Footer) -->
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

		<!--Lobster y Quatrocentro (FrontPage) -->
		<link href="https://fonts.googleapis.com/css?family=Lobster|Quattrocento" rel="stylesheet"> 
	
		<!--Playfair Display (FrontPage) -->
		<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet"> 
		
		<!-- Iconos -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
		
        <?php wp_head();?>
    </head>
    <body>
		
		<div class="w3-bar">
			<div class="w3-row">
				<div class="w3-col" style="width:125px">
					<div class="margin-t_55">
						<!-- Logo -->
						<?php
							$custom_logo_id = get_theme_mod( 'custom_logo' );
							$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
							if ( has_custom_logo() ) {
							echo '<img src="'. esc_url( $logo[0] ) .'" class="w3-left w3-white" width="130px" height="60px" alt="Logo de '. get_bloginfo( 'name' ) .'">';

							/* La variable $text_logo sirve como bandera para saber si hay una imagen de logo */
							$text_logo = false;
							} else {
							$text_logo = true;
							}
						?>
					</div> 
				</div>
				
				<div class="w3-rest">
					<div class="w3-bar w3-white border-sm-b-azulel confortaa text-bold" id="menu-responsive" >

						<!-- Menú -->						
						<?php if ( $text_logo == 1 ) { 
							echo '<span class="w3-left margin-xl_lr margin-md-t text-azulel">'. get_bloginfo( 'name' ) .'</span>';
						} 
						?>

						<?php wp_nav_menu(array(
							'theme_location' => 'superior',
							'container' => 'div',
							'container_id' => 'navbarSupportedContent',
							'items_wrap' => '<ul class="menu menu-superior no-margin margin-sm-t">%3$s</ul>',
							'menu_class' => 'w3-bar-item w3-button w3-bar-item w3-button w3-right'
						)); ?>

						<a href="javascript:void(0)" class="w3-bar-item w3-button w3-large text-azulel w3-right w3-hide-large w3-hide-medium" onclick="botonMenuResponsive()">&#9776;</a>
					</div>
				</div>
			</div>
		</div>

		<div id="mResponsive" class="w3-sidebar w3-bar-block w3-card w3-animate-right w3-hide w3-hide-large w3-hide-medium w3-center text-azulel confortaa" style="right:0;">
			<?php wp_nav_menu(array(
				'theme_location' => 'superior',
				'container' => 'div',
				'container_id' => 'navbarSupportedContent',
				'items_wrap' => '<ul class="no-margin menu" style="color:black;">%3$s</ul>',
				'menu_class' => 'w3-bar-item w3-button w3-right'
			)); ?>
		</div>
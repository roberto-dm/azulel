
<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );?>
 
<?php get_header();?>
</div>
<!-- contenido -->
<?php 
    // Query de post type frases
    $args = array( 'post_type' => 'frases', 'posts_per_page' => 15 );
    $loop = new WP_Query( $args );

    // Query de post type blog mas vistos
    $args = array(
        'posts_per_page' => 3,
        'meta_key' => 'post_views',
        'orderby' => 'meta_value_num',
        'order' => 'DESC'
    );
    $mas_vistos = new WP_Query( $args );
?>

<div id="frasesdinamicas" class="w3-image">    
    <div class="w3-row w3-image frasesdinamicas w3-display-container">
        <div class="w3-display-middle frasesdinamicas" id="FrasesDIN">
            <div class="w3-col l10 m11 w3-content w3-display-container w3-center frasesdinamicas margin-xl-t">
                <?php if ( $loop->have_posts() ) : ?>
                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <div class="w3-row w3-display-container mySlides">
                             <div class="w3-col l12 m12 s12 w3-content w3-center">
                                <a id="vinculofrases" href="<?php the_permalink(); ?>">
                                <p class="lobster sliderfrases" id="textofrases">"<?php the_title(); ?>"</p>
                                </a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
                    <?php else:  ?>
                        <p><?php echo 'No hay elementos disponibles.'; ?></p>
                    <?php endif; ?>
                    <img id="flechaizq" class="w3-display-left botonesflecha pointer" onclick="plusDivs(-1)" src="<?php echo get_stylesheet_directory_uri(); ?>/images/flechaizq.png">
                    <img id="flechader" class="w3-display-right botonesflecha marginbt pointer" onclick="plusDivs(1)" src="<?php echo get_stylesheet_directory_uri(); ?>/images/flechader.png">
            </div>
        </div>
    </div>
</div>
<div id="ContenedorBonsai">
    <img class="Bonsai" src="<?php echo get_stylesheet_directory_uri(); ?>/images/bonsai.png">
</div>
<div class="w3-row queesazulel2">
    <div class="w3-col l7 m7">
        <div class="w3-col l1 m1">.</div>
            <div class="w3-col l10 m10 queesazulel">
                <h1 class="quatrocentro tituloazulel"><b>¿Qué es <img id="logoazulelfp" src="<?php echo get_stylesheet_directory_uri(); ?>/images/azulelblanco.png">?</b></h1>
            </div>
        <div class="w3-col l1 m1"></div>
    </div>
    <div class="w3-col l5 m5"></div>
</div>
<div class="w3-row w3-margin-top w3-margin-bottom">
    <div class="w3-col l4 m4 margintop3">
        <div class="margin-sm_lr margin-b">
            <p class="w3-center Dosis ContenidoSlogan PaddingAzulel"><b><u><img class="Comillas Comillas1" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Comillas.png">"Loren ipsum wtf idk what i am writing here, try to resolve this. We are developers, we are developers, we are developers"<img class="Comillas Comillas2" src="<?php echo get_stylesheet_directory_uri(); ?>/images/Comillas2.png"></i></u></p>
        </div>    
    </div>  
    <div class="w3-col l7 m7 s10 margin-b w3-justify" id="divisorslogan">
        <p class="margin-lg-1 SourceSP" id="ContenidoSlogan2"><b>We are developers, we are developers. We are developers, we are developers. We are developers, we are developers. We are developers, we are developers. We are developers, we are developers. We are developers, we are developers. We are developers, we are developers. We are developers, we are developers. Feel the power of the code, our code. Azulel azulel azulel azulel Lorem ipsum sit amet.<br><br>Now, Loren ipsum sit amet, sit amet, play and feel. This is the main page. MAIN PAGE. Main page. </b></p>
    </div> 
</div> 
<div class="w3-row FondoTestimonios">
    <div class="w3-col l12">
        <h2 class="centrartexto Raleway LetraTestimonios">Testimonios</h2>
    </div>
    <div class="w3-col l12 Margen-TOP">
        <div class="slider1">
			<ul>
				<li>
                    <div class="w3-col l12 w3-row Contenedor Margin-b">
                        <div class="w3-col l9 m9 s12 Contenido">
                            <p class="Letrasuaves centrartexto w3-xlarge w3-text-white Raleway Interlineado1">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."</p>
                            <p class="Letrasuaves centrartexto2 w3-xlarge w3-text-white Raleway">Gerald Alarcon</p>
                        </div>
                    </div>
                </li>
				<li>
                    <div class="w3-col l12 w3-row Contenedor Margin-b">
                        <div class="w3-col l9 m9 s12 Contenido">
                            <p class="Letrasuaves w3-xlarge centrartexto w3-text-white Raleway Interlineado1">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."</p>
                            <p class="Letrasuaves w3-xlarge centrartexto2 w3-text-white Raleway">Betzabeth Linares</p>
                        </div>
                    </div>
                </li>
				<li>
                    <div class="w3-col l12 w3-row Contenedor Margin-b">
                        <div class="w3-col l9 m9 s12 Contenido">
                            <p class="Letrasuaves w3-xlarge centrartexto w3-text-white Raleway Interlineado1">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Yuju yuju yuju"</p>
                            <p class="Letrasuaves w3-xlarge centrartexto2 w3-text-white Raleway">Roberto Di Michele</p>
                        </div>
                    </div>
                </li>
				<li>
                    <div class="w3-col l12 w3-row Contenedor Margin-b">
                        <div class="w3-col l9 m9 s12 Contenido">
                            <p class="Letrasuaves w3-xlarge centrartexto w3-text-white Raleway Interlineado1">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Develop."</p>
                            <p class="Letrasuaves w3-xlarge centrartexto2 w3-text-white Raleway">Josias Gonzalez</p>
                        </div>
                    </div>
                </li>
			</ul>
		</div>
    </div>
</div>
<div id="d"> 
    <div class="w3-row Contenedor" id="divlomasvisto">
        <div class="w3-col l11 m11 Contenido" id="bordeazuls">
            <p class="w3-left playfair titulolmv text-azulel" id="titulolmv1"><b class="FirstLetter">L</b>O MÁS VISTO.</p>
    </div>
</div>
<div id="Lomasvisto">   
    <section>
        <input type="radio" name="slider-select-element" id="element1" checked="checked"/>
        <input type="radio" name="slider-select-element" id="element2"/>
        <input type="radio" name="slider-select-element" id="element3"/>            
            <div id="slider-container">    
                <div id="slider-box">
                    <div class="w3-row MargenContainer flex-cont">                      <!-- Div que separa las tarjetas -->
                        <!-- Loop de post mas vistos -->
                            <?php while ( $mas_vistos->have_posts() ) : $mas_vistos->the_post();?>                        
                                <div class="slider-element flex-cont SegmentosLMV2">                    <!-- Contenedor de cada elemento de slider en version movil -->                        
                                        <article class="element-red flex-cont">           <!-- Contenido del slider -->
                                                <div class="w3-col Contenido divslomasvisto flex-cont">    <!-- Establece tamaño de la tarjeta en version movil -->
                                                    <div class="w3-card contenidoblogsmv PR">                           <!-- Div principal de la tarjeta -->
                                                        <a href="<?php the_permalink(); ?>" style="text-decoration:none;"><div class="ImagenBlogsLMV" id="FondoBlogsLMV"> <!--Div contenedor del thumbnail con el fondo -->
                                                            <?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'img-fluid ImagenBlogsLMV' )); ?></div>
                                                        </a>
                                                        <h5 class="TituloBlogsLMV text-azulel Goudsanl"><b><a href="<?php the_permalink(); ?>" style="text-decoration:none;"><?php the_title(); ?></a></b></h5>
                                                        <div class="padding-b_50"><?php 

                                                            $sumario = get_the_excerpt();
                                                            $caracteres_sumario = strlen($sumario);
                                                            if ($caracteres_sumario > 100) {
                                                                $sumario = substr($sumario, 0, 100)."...";
                                                            }

                                                            echo '<p class="BlogsLMV goudos text-dark-grey">' . $sumario . '</p>';

                                                        ?></div>
                                                        <p class="ContenidoBlogsLMV padding-md  FechaBlogs Goudos"><b><?php echo get_the_date(); ?></b></p>
                                                    </div>
                                                </div>
                                            
                                        </article>
                                    
                                </div>
                            <?php endwhile; ?>  
                    </div>              
                </div>
            </div>
            <div id="slider-arrows" class="w3-display-container">
                <div id="labelsss">
                    <label for="element1"></label>
                    <label for="element2"></label>
                    <label for="element3"></label>
                </div>
                <img id="FlechaLMV" class="w3-display-left" src="<?php echo get_stylesheet_directory_uri(); ?>/images/flecha1.png">
                <img id="FlechaLMV" class="w3-display-right" src="<?php echo get_stylesheet_directory_uri(); ?>/images/flecha2.png">
            </div>
    </section>            
</div>




<?php get_footer();?>

 
<?php defined( 'ABSPATH' ) or die ( 'No script kiddles please!' );?>

<?php get_header();?>

<!-- Foto -->

<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
    
        <?php if ( get_the_post_thumbnail() ) { ?>
            <div class="w3-row">
                <?php 
                    $feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
                    echo '<div class="post-img w3-opacity-min w3-hover-opacity-off" style="background-image:url('.$feat_image_url.');"></div>';
                ?>
            </div>
        <?php
        } ?>
        <section class="w3-section">
            <article class="w3-container padding_0_8 post-content">
                <header>
                    <h1>
                        <?php the_title(); ?>
                    </h1>
                </header>
                <!-- contenido -->
                <?php the_content(); ?>                
            </article>
        </section>

    <?php endwhile; ?>
<?php endif; ?>

<!-- Comentarios -->
<?php comments_template(); ?>

<?php get_footer();?>
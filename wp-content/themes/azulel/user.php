<?php if (isset( $_POST['submit'] )) { //El formulario ha sido enviado
  global $reg_errors;
  $reg_errors = new WP_Error;
 
  $username = sanitize_text_field($_POST['username']);
  $email = sanitize_email($_POST['email']);
 
  //Comprobamos que el campo usuario no esté vacío
  if ( empty( $username ) ) {
    $reg_errors->add("empty-user", "El campo nombre es obligatorio");
  }
  //Comprobamos que el nombre de usuario sea mayor de 4 caracteres
  if ( 4 > strlen( $username ) ) {
    $reg_errors->add( 'username_length', 'Nombre de usuario muy corto. La cantidad mínima de caracteres es 4' );
  }
  //Comprobamos que el usuario sea válido
  if ( ! validate_username( $username ) ) {
    $reg_errors->add( 'username_invalid', 'Lo sentimos, El nombre de usuario ingresado no es válido' );
  }

  //Comprobamos que el campo email no esté vacío
  if ( empty( $email ) ) {
    $reg_errors->add("empty-email", "El campo e-mail es obligatorio");
  }  
  //Comprobamos que el email tenga un formato de email válido
  if ( !is_email( $email ) ) {
    $reg_errors->add( "invalid-email", "El e-mail no tiene un formato válido" );
  }
  //Comprobamos que el email no exista
  if ( email_exists( $email ) ) {
    $reg_errors->add( 'email', 'El email ya está registrado' );
  }

  //Verificar errores y mostrarlos 
  if ( is_wp_error( $reg_errors ) ) {
    if (count($reg_errors->get_error_messages()) > 0) {
      foreach ( $reg_errors->get_error_messages() as $error ) {
        $mensaje .= $error . "<br />";
      }
      $exitoso = false;
    }
  }

  if (count($reg_errors->get_error_messages()) == 0) {
    $password = wp_generate_password();
    // $password = $email;

    $userdata = array(
      'first_name' => $username,
      'user_login' => $email,
      'user_email' => $email,
      'user_pass' => $password
    );
    
    $user_id = wp_insert_user( $userdata );
     
     //Si todo ha ido bien, enviamos una notificación
    if ( ! is_wp_error( $user_id ) ) {
      wp_new_user_notification( $user_id, 'both' );
      $mensaje= "Suscriptor Agregado Exitosamente."; // <a href='http://localhost/azulel/wp-login.php'>¿Iniciar Sesión?</a>";
      $exitoso = true;
    }
  }
}?>
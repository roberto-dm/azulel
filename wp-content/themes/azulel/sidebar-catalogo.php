<?php // if ( is_active_sidebar('catalogo') ) : ?>
    <?php // dynamic_sidebar('catalogo') ?>
<?php // endif; ?>

<?php // Query de post type catalogo
    $args = array( 'post_type' => 'catalogo', 'posts_per_page' => 10 );
    $loop = new WP_Query( $args );
?>

<div class="w3-container">
    <div class="w3-row" id="sidebar-titulo">
        <div class="w3-col">
            <div class="w3-left">
                <h3 class="border-xs-b-azulel confortaa text-azulel text-bold"><span>Terapias</span></h3>
            </div>
        </div>
    </div>
    <div class="w3-row w3-section" id="sidebar-terapias">
        <?php if ( $loop->have_posts() ) : ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="w3-col s6 m4 l12">

                    <h5 title="<?php the_title_attribute(); ?>"><a href="<?php the_permalink(); ?>" class="w3-padding-small titulo-terapia confortaa text-azulclaro bg-hover-azulclaro w3-hover-text-white w3-ripple"><?php
                        $title = ucfirst(strtolower(get_the_title())); /* Almacena el título en la variable $title para insensibilizar el título */ /*
                        if ( strlen($title) > 15) {
                            $title = substr($title, 0, 11)."...";
                        } */
                        echo $title; 
                    ?></a></h5>
                <!--    <div class=""><?php the_excerpt(array('class'=> '')); ?></div> -->
                </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else:  ?>
            <p><?php echo 'No hay elementos disponibles.'; ?></p>
        <?php endif; ?>
    </div>
</div>
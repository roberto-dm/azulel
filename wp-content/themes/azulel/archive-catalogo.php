<?php /* Template Name: Catalogo */;?>
<?php defined( 'ABSPATH' ) or die ( 'No script kiddles please!' );?>

<?php get_header();?>

    <!-- contenido -->  

    <?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>
        
        <section class="w3-section no-margin-b" id="terapia-info">
            <header class="w3-container w3-margin-left w3-row">
                <!-- Titulo de la página-->
                <h1 class="goudsanl w3-jumbo text-azulel"><span class="border-xs-b-azulel">
                    <?php
                        $the_title = mb_strtoupper(get_the_title(), 'UTF-8'); 
                        echo $the_title;                    
                    ?>
                </span></h1>
            </header>
            
            <!-- Contenido de la página -->
            <div class="w3-container w3-margin-right w3-row">
                <div class="w3-col m10 l11 w3-mobile w3-right goudosb text-azulel text-bold text-justify interlineado-simple font-size_16">
                    <p><?php the_content(); ?></p>
                </div>           
            </div>
            <div class="w3-row">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flor_loto.png" alt="Flor de loto" class="w3-image w3-opacity-min">            
            </div>
        </section>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php // Query de post type catalogo
    $args = array( 'post_type' => 'catalogo', 'posts_per_page' => 10 );
    $loop = new WP_Query( $args );
    ?>

    <!-- Lista de Terapias Movil -->
    <section id="terapias-movil" class="flex-container w3-section no-margin-t w3-hide-medium w3-hide-large">
        
        <?php if ( $loop->have_posts() ) : ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <article class="w3-display-container">
                    <div class="w3-display-bottomright w3-container">
                        <div class="w3-row">
                            <div id="content-size-movil" class="margin-lg-t padding-lista-terapias-movil goudosb text-bold interlineado-simple contenido-tarjeta-terapia">
                                <?php 
                                    $sumario = get_the_excerpt();
                                    $caracteres_sumario=strlen($sumario);
                                    if ($caracteres_sumario > 150) {
                                        $sumario = substr($sumario, 0, 147)."...";
                                    }

                                    echo '<p>' . $sumario . '</p>';
                                ?>
                            </div>
                        </div>
                    </div>
                    <!-- Botón -->
                    <div class="w3-display-bottomright w3-container">
                        <a class="titulo-terapia w3-button w3-padding-small w3-ripple margin-xs w3-text-white w3-hover-white hover-text-azulel font-size_13 goudosb text-bold" href="<?php the_permalink(); ?>"><i>Leer más...</i></a>
                    </div>

                    <!-- Imagen del especialista -->
                    <div class="w3-display-topleft w3-container margin-lg-b" style="width: 86px;">
                        <?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'w3-image w3-circle border_3-azulel' ) ); ?>
                    </div>

                    <!-- Título o Nro de Terapia -->
                    <div class="w3-display-topright w3-container margin-md-r">
                        <header class="">
                            <h2 title="<?php the_title_attribute(); ?>"><span class="goudsanl text-bold titulo-tarjeta-terapia">
                                <a class="w3-padding-small titulo-terapia" href="<?php the_permalink(); ?>">
                                    <?php
                                        $title = ucfirst(strtolower(get_the_title())); /* Almacena el título en la variable $title para insensibilizar el título */
                                        if ( strlen($title) > 12) {
                                            $title = substr($title, 0, 9)."...";
                                        }
                                        echo $title;                                        
                                    ?>
                                </a>
                            </span></h2>
                        </header>
                    </div>                    
                </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else:  ?>
            <p><?php echo 'No hay elementos disponibles.'; ?></p>
        <?php endif; ?> 

    </section>

    <!-- Lista de terapias Tablet y Desktop-->
    <section id="terapias" class="flex-container w3-section no-margin-t w3-hide-small">
        <?php if ( $loop->have_posts() ) : ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <article class="w3-display-container">
                    <!-- Contenido -->
                    <div class="w3-display-bottomright w3-container">
                        <div class="w3-row">
                            <div id="content-size-desktop" class="w3-col m11 w3-mobile w3-right goudosb text-bold interlineado-simple padding-lista-terapias contenido-tarjeta-terapia">
                                <?php 
                                    $sumario = get_the_excerpt();
                                    $caracteres_sumario=strlen($sumario);
                                    if ($caracteres_sumario > 155) {
                                        $sumario = substr($sumario, 0, 155)."...";
                                    }

                                    echo '<p>' . $sumario . '</p>';
                                ?>
                            </div>

                        </div>
                    </div>
                    
                    <!-- Botón -->
                    <div class="w3-display-bottomright w3-container">
                        <a class="titulo-terapia w3-button w3-padding-small w3-ripple margin-xs w3-text-white w3-hover-white hover-text-azulel font-size_13 goudosb text-bold" href="<?php the_permalink(); ?>"><i>Leer más...</i></a>
                    </div>

                    <!-- Título o Nro de Terapia -->
                    <div class="w3-display-topleft w3-container">
                        <header class="margin-l_130">
                            <h2 title="<?php the_title_attribute(); ?>"><span class="w3-xxlarge goudsanl text-bold titulo-tarjeta-terapia">
                                <a class="w3-padding-small titulo-terapia" href="<?php the_permalink(); ?>">
                                    <?php
                                        $title = ucfirst(strtolower(get_the_title())); /* Almacena el título en la variable $title para insensibilizar el título */
                                        if ( strlen($title) > 12) {
                                            $title = substr($title, 0, 9)."...";
                                        }
                                        echo $title;
                                        
                                    ?>
                                </a>
                            </span></h2>
                        </header>
                    </div>

                    <!-- Nombre del especialista -->
                    <div class="w3-display-bottomleft w3-container margin-md-l">
                        <span class="w3-padding goudosb w3-text-white text-bold especialista-tarjeta-terapia">Especialista</span>
                    </div>

                    <!-- Imagen del especialista -->
                    <div class="w3-display-bottomleft w3-container margin-lg-b" style="width: 160px;">
                        <?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'w3-image w3-circle border_6-azulel' ) ); ?>
                    </div>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        <?php else:  ?>
            <p><?php echo 'No hay elementos disponibles.'; ?></p>
        <?php endif; ?> 
    </section>    
    
<?php get_footer();?>
<?php defined( 'ABSPATH' ) or die ( 'No script kiddles please!' );?>

<?php get_header();?>

<!-- Imágenes -->
<?php if ( have_posts() ) : ?>
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="w3-row">
            <section id="imagen-encabezado">
                <div class="w3-display-container">
                    <!-- Imagen de flor de loto -->
                    <div class="w3-display-topleft">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/flor_loto.png" alt="Imagen de varias flores de loto" title="Flor de loto" class="w3-image w3-opacity-min">
                    </div>
                    <!-- Imagen del Especialista -->
                    <div class="w3-display-bottomleft margin-l_40">
                        <div id="img-especialista-desktop">
                            <?php the_post_thumbnail( 'post-thumbnail', array( 'class' => 'w3-image w3-circle border_6-azulel' ) ); ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>

        <div class="w3-row">
            <!-- Contenido de una Terapia -->
            <div class="w3-col m12 l8 w3-left">
                <section id="terapia-desktop" class="w3-container contenido-terapia">            
                    <article id="info-terapia-desktop">
                        <!-- Título -->
                        <div class="w3-row">
                            <div class="w3-col">
                                <h1 class="w3-xxxlarge w3-right no-margin-t"><span class="border-xs-b-azulclaro"><?php the_title(); ?></span></h1>
                            </div>
                        </div>

                        <!-- Contenido -->
                        <div class="w3-row w3-section">
                            <div class="w3-col">
                                <?php the_content(); ?>
                            </div>
                        </div>    
                    </article>
                </section>
            </div>

            <!-- Sidebar -->
            <div class="w3-col m12 l3 w3-right">
                <?php get_sidebar("catalogo"); ?>
            </div>

        </div>

    
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer();?>
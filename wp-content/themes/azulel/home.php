<?php /* Template Name: Blog */;?>
<?php defined( 'ABSPATH' ) or die ( 'No script kiddles please!' );?>

<?php get_header();?>

    <!-- Aplicando diseños -->    
    <!-- Diseño 2 -->
	<section id="blog2" class="w3-section">
        <h2 class="goudsanm text-azulel w3-jumbo w3-center">BLOG</h2>

        <?php if ( have_posts() ) : ?>
            <div class="flex-container">
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="w3-card padding-b_50">
                        <!-- Foto -->
                        <?php if ( get_the_post_thumbnail() ) { 
                            the_post_thumbnail( 'post-thumbnail', array( 'class' => 'blogimg', 'alt' => 'Imagen alusiva a la entrada ' . get_the_title(), 'title' => get_the_title() ) );
                        }
                        else { ?> 
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/azulel.png" class="w3-image padding-xl-tb" alt="Logo de Azulel" title="Logo Azulel"> <?php
                        } ?>
                        <article class="w3-container">
                            <header class="w3-center">
                                <h5 title="<?php the_title_attribute(); ?>"><span class="goudsanl text-bold text-azulel">
                                    <a class="w3-padding-small no-text-decoration" href="<?php the_permalink(); ?>">
                                        <?php
                                            $title = mb_strtoupper(get_the_title(), 'UTF-8'); /* Almacena el título en la variable $title para insensibilizar el título */
                                            echo $title;
                                        ?>
                                    </a>
                                </span></h5>
                            </header>
                            <?php 
                                $sumario = get_the_excerpt();
                                $caracteres_sumario=strlen($sumario);
                                if ($caracteres_sumario > 100) {
                                    $sumario = substr($sumario, 0, 100)."...";
                                }

                                echo '<p class="goudos text-dark-grey">' . $sumario . '</p>';
                            ?>
                        </article>
                        <a href="<?php the_permalink(); ?>" class="goudos text-dark-grey w3-button w3-ripple bg-hover-azulel2 w3-hover-text-white w3-padding w3-round w3-right margin-sm_tb no-text-decoration leer-mas" >LEER MÁS</a>
                    </div>
                <?php endwhile; ?>
            </div>

            <!-- Paginar las entradas del blog -->
            <div class="paginacion goudos text-bold w3-center">
                <span class="w3-button w3-round bg-hover-azulel2 w3-hover-text-white text-dark-grey"><?php next_posts_link('« Entradas antiguas'); ?></span>
                <span class="w3-button w3-round bg-hover-azulel2 w3-hover-text-white text-dark-grey"><?php previous_posts_link('Entradas más recientes »'); ?></span>
            </div>

        <?php else : ?>
            <p><?php _e('Ups!, no hay entradas.'); ?></p>
        <?php endif; ?>		
	</section>

<?php get_footer();?>